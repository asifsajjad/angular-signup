import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { AccessCheckerService } from './services/access-checker.service';

const routes: Routes = [
  {
    path: '',
    title: "Signin/Signup",
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    title: "dashboard",
    resolve: {
      resolvedData: AccessCheckerService
    },
    component: DashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
