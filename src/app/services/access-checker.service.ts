import { Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { StorageService } from './storage.service';


@Injectable({
    providedIn: 'root',
})
export class AccessCheckerService implements Resolve<null> {
    constructor(private router: Router, private storageService: StorageService) {}

    resolve(route: ActivatedRouteSnapshot, rstate: RouterStateSnapshot) {
        const loggedInUser = this.storageService.getData("ACTIVE_USER");
        if(!loggedInUser){
            this.router.navigate(['']);
        } 
            return null;
                  
    }
}