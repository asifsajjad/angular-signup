import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { StorageService } from 'src/app/services/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(private storageService: StorageService, private router: Router) {}

  storedUserData: string = '';
  ngOnInit(): void {
    const loggedInUser = this.storageService.getData('ACTIVE_USER');
    if (loggedInUser) {
      this.router.navigate(['dashboard']);
    }
    this.storedUserData = this.storageService.getData('userData') || '[]';
    // persist state
    this.signIn =
      (this.storageService.getData('signInState') || 'true') === 'true';
    this.signUp =
      (this.storageService.getData('signUpState') || 'false') === 'true';
  }

  wrongCredentials = false;
  presentUserName = false;
  presentUserEmail = false;
  rePassword: string = '';
  invalidSignUpDetails = true;
  badEmail = false;
  hide = true;
  signUpFailed = false;
  user = new User('', '', '', '', '');
  submitted = false;
  badPassword= false;

  checkUserName = () => {
    console.log('changed username:', this.user);
    const users = this.storedUserData;
    console.log(users);
    const usersArray = JSON.parse(users || '[]');
    const user = usersArray
      ?.filter((userObj: User) => userObj.uname === this.user.uname)
      ?.pop();
    if (user) {
      this.presentUserName = true;
    } else {
      this.presentUserName = false;
    }
  };

  checkEmail = () => {
    console.log('changed email:', this.user);
    const users = this.storedUserData;
    const usersArray = JSON.parse(users || '[]');
    const user = usersArray
      ?.filter((userObj: User) => userObj.email === this.user.email)
      ?.pop();
    if (user) {
      this.presentUserEmail = true;
    } else {
      this.presentUserEmail = false;
    }
    const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    if (!emailRegex.test(this.user.email)) {
      this.badEmail = true;
    } else {
      this.badEmail = false;
    }
  };

  checkPassword = () => {
    if(this.user.password.length <8){
      this.badPassword = true;
    } else {
      this.badPassword = false;
    }
    return;
  }

  onSignUp() {
    this.submitted = true;
    console.log('On submit pressed!');
    if (
      this.presentUserEmail ||
      this.presentUserName ||
      this.rePassword !== this.user.password ||
      this.badEmail
    ) {
      this.signUpFailed = true;
      return;
    }
    const users = this.storageService.getData('userData');
    const usersArray = JSON.parse(users || '[]');
    usersArray.push(this.user);
    const newusers = JSON.stringify(usersArray);
    const setData = this.storageService.saveData('userData', newusers);
    const persistLogin = this.storageService.saveData(
      'ACTIVE_USER',
      JSON.stringify(this.user)
    );
    this.clearState();
    this.router.navigate(["dashboard"]);
  }

  onSignIn() {
    console.log('On signin pressed');
    const users = this.storedUserData;
    const usersArray = JSON.parse(users || '[]');
    const user = usersArray
      ?.filter((userObj: User) => userObj.uname === this.user.uname)
      ?.pop();
    if (!user) {
      this.wrongCredentials = true;
      return;
    } else {
      if (user.password === this.user.password) {
        this.wrongCredentials = false;
        this.storageService.saveData('ACTIVE_USER', JSON.stringify(user));
        this.clearState();
        this.router.navigate(['dashboard']);
      } else {
        this.wrongCredentials = true;
        return;
      }
    }
    return;
  }

  signIn: boolean = true;
  signUp: boolean = false;

  redirectToSignUp = () => {
    this.signIn = false;
    this.signUp = true;
    // Persist state
    this.storageService.saveData('signInState', 'false');
    this.storageService.saveData('signUpState', 'true');
  };

  redirectToSignIn = () => {
    this.signIn = true;
    this.signUp = false;
    // Persist state
    this.storageService.saveData('signInState', 'true');
    this.storageService.saveData('signUpState', 'false');
  };

  clearState = () => {
    this.storageService.removeData("signInState");
    this.storageService.removeData("signUpState");
  }
}
