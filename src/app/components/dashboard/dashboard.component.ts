import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { StorageService } from 'src/app/services/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private storageService: StorageService, private router: Router) {};

  stringifiedUser = "";
  user:User = {
    uname: "",
    firstName: "",
    lastName: "",
    email: "",
    password: "",
  }
  ngOnInit() {
    this.stringifiedUser = this.storageService.getData("ACTIVE_USER")|| "{}";
    this.user = JSON.parse(this.stringifiedUser);
  }

  logOut(){
    this.storageService.removeData("ACTIVE_USER");
    this.router.navigate([""]);
  }


}
